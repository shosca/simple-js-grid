(($) ->
  methods =
    dohandleresize: ->
      @w = @width()
      @h = @height()
      @gridwidth = @w - @fixedwidth
      @headheight = @fixedcolumnheadertable.height()
      @gridheight = @h - @headheight

      @container.css(
        width: @w
        height: @h
        position: "relative"
      )

      @fixedcolumncontainer.css(
        "overflow-x": "scroll"
        height: @h + 1
        width: @fixedwidth
        "overflow-y": "hidden"
        position: "absolute"
        top: 0
        left: 0
      )
      @seperator.css(
        height: @h + 1
        width: 4
        "overflow-y": "hidden"
        "overflow-x": "hidden"
        position: "absolute"
        top: 0
        left: @fixedwidth
        'z-index': 1
      )

      @fixedcolumnheader.css(
        overflow: "hidden"
        height: @headheight
        width: @columnwidth
        position: "relative"
      )

      @fixedcolumnheadertable.css(
        width: @columnwidth
        "table-layout": "fixed"
      )

      @fixedcolumn.css(
        width: @columnwidth
        height: @gridheight
        "overflow-x": "scroll"
        "overflow-y": "hidden"
      )

      @fixedcolumntable.css(
        width: @columnwidth
        "table-layout": "fixed"
      )

      @gridcontainer.css(
        top: 0
        position: "absolute"
        width: @gridwidth
        height: @h
        left: @fixedwidth
      )

      @gridheadercontainer.css(
        width: @gridwidth
        height: @headheight
        overflow: "hidden"
        position: "relative"
      )

      @grid.css(
        width: @gridwidth
        "overflow-x": "scroll"
        "overflow-y": "auto"
        height: @gridheight
      )

    setcolumnwidth: (mousedownevent) ->
      @lastmousepos = mousedownevent.clientX + document.documentElement.scrollLeft;
      @seperatoroffset = @columnwidth - @lastmousepos
      @seperator.addClass('move')
      $(document).disableSelection().bind 'mousemove.gridseperator', (e) =>
        @columnwidth = @seperatoroffset + e.clientX + document.documentElement.scrollLeft;
        @fixedwidth = @columnwidth
        methods.dohandleresize.apply this

      $(document).bind 'mouseup', =>
        @seperator.removeClass('move')
        $(document).enableSelection().unbind 'mousemove.gridseperator'


    handlescroll: ->
      @fixedcolumn.scrollTop @grid.scrollTop()
      @gridheadercontainer.scrollLeft @grid.scrollLeft()

    syncrows: ->
      rows = @gridtable.find("tr")
      fixedrows = @fixedcolumntable.find("tr")
      i = 0
      r = rows.length

      t = (r, f, h, i) ->
        setTimeout(->
          r.style.height = h + "px"
          f.style.height = h + "px"
        ,i)

      while i < r
        row = rows[i]
        frow = fixedrows[i]
        rh = row.offsetHeight
        fh = frow.offsetHeight
        if rh != fh
          h = Math.max(fh, rh)
          t.call(null, row, frow, h, i)

        i++

      return null

    synccolumns: ->
      cells = @gridtable.find("tr:first td")
      headercells = @gridheadertable.find("td, th")
      i = 0
      cols = cells.length

      while i < cols
        cell= cells[i]
        hcell = headercells[i]
        cw = cell.offsetWidth
        hw = hcell.offsetWidth
        if cw != hw
            if cw < hw
              cell.style.width = hw + 'px'
              hcell.style.width = hw + 'px'
            else
              cell.style.width = cw + 'px'
              hcell.style.width = cw + 'px'
        i++

      return null

    init: () ->
      self = this
      @$el = $ this.element
      @gridtable = $(this.element.cloneNode(false))
      @gridheadertable = $(this.element.cloneNode(false)).removeAttr("id")
      @fixedcolumntable = $(this.element.cloneNode(false)).removeAttr("id")
      @fixedcolumnheadertable = $(this.element.cloneNode(false)).removeAttr("id")

      @scrollbarwidth = @scrollbarWidth || 17
      @w = @width()
      @h = @height()
      @fixedcols = @fixedcols || 1
      if not @fixedwidth
        @fixedwidth = 0
        for c in @$el.find('tr:first th, tr:first td').slice(0, @fixedcols)
          @fixedwidth += c.offsetWidth

      @$el.hide()
      parent = @$el.parent()
      @$el.remove()

      @container = $("<div class='grid_container' />")

      @fixedcolumncontainer = $("<div class='grid_columncontainer' />").appendTo @container

      @seperator = $("<div class='grid_seperator' style='cursor: col-resize;' />").appendTo @container

      @fixedcolumnheader = $("<div class='grid_columnheader' />")
        .appendTo @fixedcolumncontainer

      @fixedcolumnheadertable.appendTo @fixedcolumnheader

      @fixedcolumn = $("<div class='grid_column' />").appendTo @fixedcolumncontainer


      @fixedcolumntable.appendTo @fixedcolumn

      @gridcontainer = $("<div class='grid_gridcontainer' />").appendTo @container

      @gridheadercontainer = $("<div class='grid_gridheadercontainer' />").appendTo @gridcontainer

      @gridheadertable.appendTo @gridheadercontainer

      @grid = $("<div class='grid_grid' />")
        .bind("scroll", this, (event) ->
          methods.handlescroll.apply event.data
        ).appendTo @gridcontainer

      @gridtable.appendTo @grid
      @container.appendTo parent

      methods.buildrows.call this, @$el.find('thead tr'), @fixedcolumnheadertable, @gridheadertable
      methods.buildrows.call this, @$el.find('tbody tr'), @fixedcolumntable, @gridtable

      @headheight = @headheight || @fixedcolumnheadertable.height()
      @fixedcolumnheader.css
          height: @headheight

      @columnwidth = @columnwidth || @fixedwidth
      @gridheight = @h - @headheight
      @gridwidth = @w - @fixedwidth
      methods.dohandleresize.apply this

      methods.synccolumns.apply this
      methods.syncrows.apply this
      @gridheadertable.css('table-layout', 'fixed')
      @gridtable.css('table-layout', 'fixed')

      @seperator.bind 'mousedown', (event) =>
        methods.setcolumnwidth.call this, event

      if @handleresize and @handleresize is true
        $(window).bind "resize", (event) =>
          methods.dohandleresize.apply this

    buildrows: (rows, fixed, grid) ->
      fixedrows = []
      gridrows = []
      f = 0
      g = 0
      for tr in rows
        fixedrows[f++] = "<tr "
        gridrows[g++] = "<tr "
        for k in tr.attributes
          fixedrows[f++] = k.name
          fixedrows[f++] = "='"
          fixedrows[f++] = k.value
          fixedrows[f++] = "'"
          gridrows[g++] = k.name
          gridrows[g++] = "='"
          gridrows[g++] = k.value
          gridrows[g++] = "'"

        fixedrows[f++] = " >"
        gridrows[g++] = " >"

        for cell, i in $(tr).children('td, th').toArray()
          if i < @fixedcols
            fixedrows[f++] = cell.outerHTML
          else
            gridrows[g++] = cell.outerHTML

        fixedrows[f++] = "</tr>"
        gridrows[g++] = "</tr>"

      fixed.append(fixedrows.join(''))
      grid.append(gridrows.join(''))
      return null

  $.fn.grid = (method) ->
    if methods[method]
      $(this).each ->
        methods[method].apply this, Array::slice.call(arguments, 1)
    else if typeof method is "object" or not method
      $(this).each ->
        method.element = this
        $(this).data 'opts', method
        methods.init.apply method
    else
      $.error "Method " + method + " does not exist on jQuery.grid"
) @jQuery
